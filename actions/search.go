package actions

import (
	"strings"

	"bitbucket.org/elsammons/gojenkins"
)

/*
 Find Jobs based on some portion of a name provided.
*/
func SearchJobsByName(jobs []gojenkins.Jobber, search string) []string {
	updatedJobs := make([]string, 0, 1000)

	for _, job := range jobs {
		if strings.Contains(job.Name, search) {
			updatedJobs = append(updatedJobs, job.Name)
		}
	}

	return updatedJobs
}
