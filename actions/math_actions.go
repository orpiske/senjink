package actions

func Sum(x []float64) float64 {
	sum := 0.0
	for _, v := range x {
		sum += v
	}
	return sum
}

func CalculatePercent(n, d float64) float64 {
	solution := 0.0
	solution = ((n / d) * 100)
	return solution
}
